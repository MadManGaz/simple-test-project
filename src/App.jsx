import { useState } from "react";
import "./App.css";

function ActionButton({ isSmall, onClick, children }) {
  return (
    <button
      style={isSmall ? { padding: "10px", fontSize: "medium" } : {}}
      className="action-button"
      onClick={onClick}
    >
      {children}
    </button>
  );
}

function Counter() {
  const [count, setCount] = useState(0);

  const incrementCount = () => {
    setCount(count + 1);
  };

  return (
    <div>
      <h1>{count}</h1>
      <ActionButton onClick={incrementCount} isSmall>
        Increment
      </ActionButton>
    </div>
  );
}

function App() {
  return (
    <div className="App">
      <h1>HELLO</h1>
      <Counter />
    </div>
  );
}

export default App;
